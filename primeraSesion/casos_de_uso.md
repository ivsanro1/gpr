
## Descripción de casos de uso ##

A continuación se describen los casos de uso más relevantes

**Caso de Uso “Alta paciente”**
  ---
  > ID            > 1
  ---
  > Caso de Uso   > Alta paciente
   
  > Actores       > OperadorEmergencia (Iniciador)

  > Propósito     > Dar de alta un nuevo paciente en el sistema de emergencias

  > Resumen       > El sistema pregunta los datos necesarios del paciente.
                  >
                  > El sistema comprueba que el paciente no existe y en caso contrario lo crea.

  > Precond       > -

  > Postcond      > El nuevo paciente se da de alta en el sistema
  ---

**Caso de Uso “Cambiar disponibilidad de ambulancia”**

  ---
  > ID            > 2
  --- 
  > Caso de Uso   > Cambiar disponibilidad de una ambulancia

  > Actores       > ConductorAmbulancia (Iniciador)

  > Propósito     > Cambiar la disponibilidad de una ambulancia

  > Resumen       > -
  > 
  - El actor introduce el número de registro de la ambulancia.
  - El sistema localiza la ambulancia deseada y muestra sus datos en pantalla (entre ellos la disponibilidad)
  -El actor pulsa un botón llamado “Cambiar disponibilidad”
  - El sistema almacena la nueva disponibilidad

  > Precond       > -

  > Postcond      > La disponibilidad de la ambulancia ha cambiado
  ---

**Caso de Uso “Cambiar coordenadas de ambulancia”**
  ---
  > ID            3
  ---
  > Caso de Uso   > Cambiar coordenadas de una ambulancia

  > Actores       > ConductorAmbulancia (Iniciador)

  > Propósito     > Cambiar las coordenadas de una ambulancia

  > Resumen       > -
  > 
  - El actor introduce el número de registro de la ambulancia.
  - El sistema localiza la ambulancia deseada y muestra sus datos en pantalla (entre ellos las coordenadas antiguas).
  - El actor cambia las coordenadas y pulsa un botón llamado “Cambiar coordenadas”.
  - El sistema almacena las nuevas coordenadas.

  > Precond       > -

  > Postcond      > Las coordenadas de la ambulancia han cambiado
  ---
   
**Caso de Uso “Listado especialidades médicas de cada hospital”**
  ---
  > ID            4
  ---
  > Caso de Uso   > Listado especialidades médicas de cada hospitales

  > Actores       > PersonalComisión (Iniciador)

  > Propósito     > Consultar las especialidades médicas disponibles en cada                   hospital

  > Resumen       > -
  >
  - Mientras el actor no pulse el botón “salir”
  - El actor selecciona en un ComboBox de entre todos los hospitales el que le interesa.
  - El sistema busca y muestra las especialidades del hospital seleccionado en una tabla.
  
  > Precond       > -

  > Postcond      > -
  ---

**Caso de Uso “Listado pacientes”**
  ---  
  > ID            > 5
  ---
  > Caso de Uso   > Listado pacientes

  > Actores       > PersonalComisión (Iniciador)

  > Propósito     > Consultar los pacientes

  > Resumen       > El sistema busca y muestra los pacientes en una tabla.

  > Precond       > -

  > Postcond      > -
  ---

**Caso de Uso “Nueva Emergencia”**
  ---
  > ID            > 6
  --- 
  > Caso de Uso   Nueva Emergencia

  > Actores       OperadorEmergencia (Iniciador)

  > Propósito     Dar de alta una nueva emergencia.

  > Resumen     > -
  >
  >
  - El sistema solicita el dni o el nombre del paciente.
  - El actor introduce el campo apropiado y busca el paciente.
  - Si el paciente no está dado de alta se pedirán todos los datos y se dará de alta en el sistema.
  - El sistema muestra los datos del paciente en pantalla.
  - El sistema solicita las coordenadas del paciente.
  - El actor las introduce.
  - Mientras queden síntomas por añadir:
  - El sistema solicita una especialidad y pregunta por el síntoma que presenta el paciente.
  - El actor introduce la información de la especialidad y el síntoma.
  - El sistema almacena toda la información en objetos en memoria, teniendo en cuenta las relaciones entre clases.
  - El sistema calcula y almacena la mejor asignación (hospital + ambulancia) para la llamada de emergencia, de acuerdo a la expresión y restricciones definidas en el caso de estudio.
  - Visualizar un mensaje de notificación con la ambulancia y hospital asignado.

>   Nota: Puede ocurrir que no se encuentren ambulancias disponibles o que no existan hospitales que den soporte a ciertas especialidades. Si esto ocurre, se debe también visualizar un mensaje de notificación.

  > Precond       > -
 
  > Postcond      > Un nuevo registro de emergencia queda registrado en el sistema. 
  ---

**Caso de Uso “Listado de llamadas de emergencia”**
  ---
  > ID            > 7
  ---
  > Caso de Uso   > Listado de llamadas de emergencia
  
  > Actores       > PersonalComisión (Iniciador)

  > Propósito     > Conocer las llamadas que se ha realizado.

  > Resumen       > El sistema mostrará todas las llamadas de emergencia que se hayan realizado incluyendo la información del paciente atendido, los síntomas que tenía, el hospital asignado y los datos de la ambulancia asignada.
  
  > Precond       -

  > Postcond      -
  ---