#SERVICIO DE LLAMADAS DE EMERGENCIAS#

La Comisión Nacional de Emergencias ha contratado a la empresa "ISG
Software" para desarrollar un nuevo software que automatice un servicio
de llamadas de emergencia. La primera campaña se centrará únicamente en
los servicios de atención de urgencia prestados por ambulancias, aunque
en el futuro podrían proporcionarse también otros servicios.

El software que se desarrollará en esta campaña debe incluir toda la
información requerida por la Comisión, como los hospitales, las
especialidades médicas, las ambulancias, las llamadas de emergencia, los
pacientes, etc. Además, se requiere que la asignación de ambulancias y
hospitales ante una situación de emergencia se realice de la forma más
eficiente posible.

##Descripción general:##

La Comisión quiere ofrecer un servicio de llamadas de emergencias
eficiente que optimice, cuando ocurra una emergencia, la llegada de las
ambulancias, y el posterior transporte de los pacientes a los hospitales
más apropiados. Para abordar este problema, se ha facilitado la
descripción de los siguientes elementos:

-   **Hospitales**. Cada hospital tiene un nombre, una dirección y unas
    > coordenadas geográficas (latitud y longitud, p.e. 39.451, -0.358).

-   **Especialidades médicas**. Cada especialidad tiene un nombre (p.e.
    > dermatología, cardiología, cirugía, etc.). No todas las
    > especialidades son ofrecidas por todos los hospitales; pero cada
    > hospital ofrece al menos una especialidad, y todas las
    > especialidades son ofrecidas por al menos un hospital.

-   **Ambulancias**. Cada ambulancia tiene una placa o número de registro,
    > un tipo de equipo instalado y las coordenadas geográficas dadas
    > por un GPS para saber dónde se encuentra en cada momento. Hay que
    > tener en cuenta que las ambulancias pueden moverse y sus
    > coordenadas pueden cambiar a lo largo del tiempo, así como su
    > disponibilidad; evidentemente si una ambulancia está ocupada por
    > un paciente no está disponible. Hay dos tipos de ambulancias:
    > ambulancias privadas y ambulancias con base en los hospitales. Las
    > primeras no se asignan a ningún hospital particular y pueden
    > transportar a los pacientes a cualquier hospital del sistema. Las
    > últimas se asignan sólo a un hospital y pueden transportar a los
    > pacientes sólo a ese hospital en particular. En el sistema habrá
    > hospitales que tienen ambulancias y hospitales que no las tienen.

-   **Pacientes**. Cada persona que se almacena en el sistema ha marcado el
    > número de emergencias al menos una vez, con lo que se convierte
    > realmente en un paciente. De lo contrario, aún no forma parte del
    > sistema. Los campos más interesantes que describen a un paciente
    > son su DNI, nombre y apellidos, sexo, edad, número de teléfono y
    > dirección.

-   **Llamadas de emergencia**. Los pacientes marcan el número de teléfono
    > de emergencias en caso de emergencia. Se quiere almacenar las
    > coordenadas geográficas del lugar donde ocurre la emergencia,
    > proporcionadas automáticamente por la compañía telefónica, para
    > averiguar la combinación óptima de ambulancia y hospital para
    > transportar al paciente. Una emergencia significa que el paciente
    > muestra algunos signos y síntomas relacionados con alguna de las
    > especialidades (el paciente presenta al menos un síntoma de alguna
    > de las especialidades médica, pero podría ser más de uno). Para
    > cada uno de los síntomas que presenta un paciente es necesario
    > conocer su gravedad (alta, media o baja) y durante cuánto tiempo
    > (en horas) el paciente está sufriendo ese síntoma.

-   **Asignación** de un paciente a una ambulancia y a un hospital. Después
    > de recibir una llamada de emergencia, el sistema tiene que asignar
    > automáticamente la ambulancia y hospital óptimos para ese
    > paciente. La asignación óptima se calcula en función de la
    > distancia más corta entre una ambulancia disponible (es decir, no
    > ocupada en ese momento por un paciente) el paciente y un hospital
    > al que sea posible transportarlo, teniendo en cuenta que las
    > ambulancias con base en un hospital, sólo pueden ir a ese
    > hospital.

##Funcionamiento del sistema:##

Cuando una persona marca el número de emergencia, un operador
especialmente preparado contesta la llamada y completa un formulario
electrónico con la información sobre: ​​i) el paciente y sus datos, si
se trata de la primera vez que utiliza el sistema; ii) las coordenadas
geográficas donde se encuentra el paciente y se ha producido la
emergencia; y iii) todos los síntomas, sus respectivas especialidades e
información adicional acerca de su gravedad y duración. Una vez que toda
esta información se ha introducido en el sistema, éste tiene que
proporcionar automáticamente una ambulancia y un hospital para hacer
frente a esa emergencia de una manera muy eficiente. Es importante tener
en cuenta las siguientes restricciones:

-   El hospital asignado a una emergencia tiene que ofrecer todas las
    > especialidades necesarias para tratar dicha emergencia.

-   La ambulancia asignada a la emergencia debe estar disponible y debe
    > poder transportar al paciente a ese hospital, bien porque es una
    > ambulancia privada (y puede, consecuentemente, transportar
    > pacientes a cualquier hospital del sistema), o bien porque tiene
    > base en ese hospital.

-   Las ambulancias pueden circular con o sin pacientes, por lo que sus
    > coordenadas geográficas pueden variar en cualquier momento. Por lo
    > tanto, es necesario que el conductor de la ambulancia sea capaz de
    > cambiar fácilmente esas coordenadas de forma manual. Además, se
    > asumirá que una ambulancia está siempre disponible, salvo que esté
    > asignada en ese momento a una emergencia. Igualmente, es necesario
    > que el conductor disponga de una opción para cambiar la
    > disponibilidad de su ambulancia, por ejemplo, cuando una
    > ambulancia ha entregado un paciente en un hospital y, por lo
    > tanto, vuelve a estar disponible.

-   La asignación óptima de la ambulancia y el hospital se calcula por
    > medio de la distancia euclídea, ya que se considera que todas las
    > ambulancias tienen la misma velocidad media. De esta manera, el
    > sistema devolverá la tupla \<ambulancia~i~, hospital~j~\> que
    > minimice la siguiente expresión:

$$\sum_{\forall\ \text{ambulancia}_{i},\ \text{hospital}_{j}}^{}\begin{matrix}
\text{distancia}\left( {coordenadas\_ ambulancia}_{i},coordenadas\_ emergencia \right) + \\
distancia(coordenadas\_ emergencia,{coordenadas\_ hospital}_{j}) \\
\end{matrix}$$

El sistema debe gestionar todo lo descrito anteriormente. Además, debe
proporcionar al administrador del sistema las operaciones CRUD
habituales (Create, Read, Update y Delete) para hospitales,
especialidades médicas, ambulancias, pacientes, etc. Las búsquedas de
hospitales, especialidades, ambulancias y pacientes con respecto a sus
atributos más relevantes también serán necesarias. Por último, el
sistema proporcionará los siguientes informes para el responsable de la
Comisión, los directores de hospitales y los conductores de las
ambulancias:

-   Especialidades médicas de cada hospital (para el responsable de la
    Comisión).

-   Especialidades médicas del hospital (para el director del hospital).

-   Pacientes del sistema (para el responsable de la Comisión) y
    pacientes que han sido asignados a cada hospital (para el director
    del hospital).

-   Ambulancias con base en hospitales para cada hospital (para el
    director del hospital).

-   Llamadas de emergencia, con la información sobre el paciente, los
    síntomas y especialidades, y con la asignación de ambulancia y
    hospital (para el responsable de la Comisión).

-   Llamadas de emergencia asignadas y hospital destino (para los
    conductores de ambulancias).

-   Hospitales y ambulancias que han servido el mayor y/o el menor
    número de llamadas de emergencia (para el responsable de la
    Comisión). Esta información se considera esencial para mejorar la
    prestación del servicio.

##ESPECIFICACIÓN DE CASOS DE USO##

Diagrama de contexto
====================

El diagrama de contexto se muestra en la siguiente figura:

![](https://bitbucket.org/magomar/gpr-demo/raw/master/caso_estudio/media/ diagrama_contexto.png)

La funcionalidad ofertada por el sistema se puede agrupar en paquetes,
de modo que el modelo inicial queda reflejado en la siguiente figura.

![](https://bitbucket.org/magomar/gpr-demo/raw/master/caso_estudio/media/paquetes.png)

A continuación mostraremos el contenido de cada paquete.

Gestión de emergencias
======================

La funcionalidad de la gestión de una emergencia contiene los casos de
uso que permiten atender a una llamada de emergencia (nueva emergencia),
cambiar la posición de una ambulancia y cambiar el estado de la misma.

![](https://bitbucket.org/magomar/gpr-demo/raw/master/caso_estudio/media/operador_emergencia.png)

Operaciones conductor
=====================

El paquete contiene la funcionalidad del conductor de ambulancia

![](https://bitbucket.org/magomar/gpr-demo/raw/master/caso_estudio/media/conductor_ambulancia.png)

Listados personal comisión y hospital
=====================================

El paquete contiene los diversos listados del sistema.

![](https://bitbucket.org/magomar/gpr-demo/raw/master/caso_estudio/media/listados.png)

Mantenimiento
=============

Contiene los casos de uso que se encargan de mantener la información del
sistema: hospitales, ambulancias, especialidades y pacientes.

![](https://bitbucket.org/magomar/gpr-demo/raw/master/caso_estudio/media/administrador.png)

Observe que en la figura no se muestra el alta de paciente, hemos
supuesto que únicamente los puede dar de alta el personal de emergencia
como resultado de una llamada de emergencia.